package com.regeneration.academy.web.pp.controller.repair;

import com.regeneration.academy.web.pp.forms.RepairForm;
import com.regeneration.academy.web.pp.model.RepairModel;
import com.regeneration.academy.web.pp.service.RepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class RepairEditController {

    private static final String REPAIR_FORM = "repairForm";
    private static final String REPAIR= "repair";

    @Autowired
    private RepairService repairService;


    @PostMapping(value = "/admin/repairs/{id}/delete")
    public String deleteRepair(@PathVariable Long id, Model model) {
        repairService.deleteById(id);
        return "redirect:/admin/repairs";
    }

    @PostMapping(value = "/admin/repairs/{id}/edit")
    public String editRepair(@PathVariable Long id, Model model) {
        RepairModel repairModel = repairService.findById(id).get();
        model.addAttribute(REPAIR_FORM, new RepairForm());
        model.addAttribute(REPAIR, repairModel);
        return "pages/repair_edit";
    }

    @PostMapping(value = "/admin/repairs/edit")
    public String editRepair(RepairModel repairModel) {
        repairService.updateRepair(repairModel);
        return "redirect:/admin/repairs";
    }
}
