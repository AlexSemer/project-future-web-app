package com.regeneration.academy.web.pp.controller;

import com.regeneration.academy.web.pp.model.PropertyModel;
import com.regeneration.academy.web.pp.model.RepairModel;
import com.regeneration.academy.web.pp.model.ReportModel;
import com.regeneration.academy.web.pp.model.UserModel;
import com.regeneration.academy.web.pp.service.PropertyService;
import com.regeneration.academy.web.pp.service.RepairService;
import com.regeneration.academy.web.pp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;
import java.util.Optional;

@Controller
public class OwnerController {

    private static final String USER_DETAILS = "userDetails";
    private static final String USER_PROPERTIES = "userProperties";
    private static final String USER_REPAIRS = "userRepairs";
    private static final String REPORT_MODEL = "reportModel";

    @Autowired
    private UserService userService;

    @Autowired
    private PropertyService propertyService;

    @Autowired
    private RepairService repairService;

    @GetMapping(value = "/user")
    public String repairsToday(Model model) {
        long id = 1;

        Optional<UserModel> userDetails = userService.findById(id);
        String ssn = "348972198";
        List<PropertyModel> userProperties = propertyService.findByOwner(ssn);


        List<RepairModel> userRepairs = repairService.findByOwner(ssn);
        ReportModel reportModel = new ReportModel();
        reportModel.setPendingRepairs(80.0);
        reportModel.setInProgressRepairs(15.0);
        reportModel.setCompletedRepairs(5.0);
        //model.addAttribute(USER_DETAILS, userDetails);
        model.addAttribute(USER_PROPERTIES, userProperties);
        model.addAttribute(USER_REPAIRS, userRepairs);
        model.addAttribute(REPORT_MODEL, reportModel);
        return "pages/owner";
    }

}
