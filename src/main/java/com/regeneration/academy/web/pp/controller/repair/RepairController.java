package com.regeneration.academy.web.pp.controller.repair;

import com.regeneration.academy.web.pp.model.RepairModel;
import com.regeneration.academy.web.pp.service.RepairService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Controller
public class RepairController {
    private static final String REPAIR_LIST = "repairs";
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private RepairService repairService;

    @GetMapping(value = "/admin/repairs")
    public String repairs(Model model) {
        List<RepairModel> repairs = repairService.findAll();
        model.addAttribute(REPAIR_LIST, repairs);
        return "pages/repairs_show";
    }

    @GetMapping(value = "/admin")
    public String repairsToday(Model model) {
        DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String sDate3="2018-12-30";
        LocalDate dateToday = LocalDate.parse(sDate3, DATE_TIME_FORMATTER);
        List<RepairModel> repairs = repairService.findTop10ByDateOrderByDateAsc(dateToday);
        model.addAttribute(REPAIR_LIST, repairs);
        return "pages/admin";
    }



    public void findAll() {
        repairService.findAll().forEach(repair -> logger.info(repair.toString()));
    }

    public void findBySsn(String ssn){
        repairService.findByOwner(ssn).forEach(repair -> logger.info(repair.toString()));
    }
    public void findByType(String type) {
        repairService.findByType(type).forEach(repair -> logger.info(repair.toString()));
    }

    public void findByDateAfter(LocalDate date){
        repairService.findByDateAfter(date).forEach(repair -> logger.info(repair.toString()));
    }

    public void findByDateBefore(LocalDate date){
        repairService.findByDateBefore(date).forEach(repair -> logger.info(repair.toString()));
    }

    public void findByOwnerAndDateBefore(String owner, LocalDate dateBefore){
        repairService.findByOwnerAndDateBefore(owner, dateBefore).forEach(repair -> logger.info(repair.toString()));
    }

    public void findByOwnerAndDateAfter(String owner, LocalDate dateAfter){
        repairService.findByOwnerAndDateAfter(owner, dateAfter).forEach(repairModel -> logger.info(repairModel.toString()));
    }

    public void findByOwnerAndDateBetween(String owner, LocalDate dateBefore, LocalDate dateAfter){
        repairService.findByOwnerAndDateBetween(owner, dateBefore, dateAfter).forEach(repairModel -> logger.info(repairModel.toString()));
    }

    public void findTodaysServices(LocalDate date){
        repairService.findTop10ByDateOrderByDateAsc(date).forEach(repairModel -> logger.info(repairModel.toString()));
    }



}