package com.regeneration.academy.web.pp.controller.property;

import com.regeneration.academy.web.pp.forms.PropertyForm;
import com.regeneration.academy.web.pp.model.PropertyModel;
import com.regeneration.academy.web.pp.service.PropertyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class PropertyEditController {


    private static final String PROPERTY_FORM = "propertyForm";
    private static final String PROPERTY = "property";

    @Autowired
    private PropertyService propertyService;

    @PostMapping(value = "/admin/properties/{id}/edit")
    public String editProperty(@PathVariable Long id, Model model) {
        PropertyModel propertyModel = propertyService.findById(id).get();
        model.addAttribute(PROPERTY_FORM, new PropertyForm());
        model.addAttribute(PROPERTY, propertyModel);
        return "pages/property_edit";
    }

    @PostMapping(value = "/admin/properties/edit")
    public String editProperty(PropertyModel propertyModel) {
        propertyService.updateProperty(propertyModel);
        return "redirect:/admin/properties";
    }

    @PostMapping(value = "/properties/{id}/delete")
    public String deleteProperty(@PathVariable Long id, Model model) {
        propertyService.deleteById(id);
        return "redirect:/admin/properties";
    }
}
