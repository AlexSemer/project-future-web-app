-- https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-sql.html
-- https://docs.spring.io/spring-boot/docs/current/reference/html/howto-database-initialization.html
-- IN THIS FILE WE CAN WRITE AN SQL SCRIPT CONTAINING:
-- SCHEMA, TABLE AND DATA MANIPULATION QUERIES
-- TO BE EXECUTED AUTOMATICALLY DURING THE INITIALIZATION OF THE APPLICATION
-- AND AFTER THE CREATION OF SCHEMA AND TABLES BY Hibernate
-- IF spring.jpa.hibernate.ddl-auto IS SET TO create OR create-drop
-- IT IS A Hibernate feature (nothing to do with Spring)



--INSERT INTO USER (user_ssn, user_first_name, user_last_name, user_address, user_email, user_phone, user_username, user_password, user_role) VALUES ('348972198' , 'John', 'Smith', '1234 Main St, Athens, Greece', 'john.smith@gmail.com', '2101234567', 'john_smith', '123', 'Owner')
INSERT INTO USER (user_ssn, user_first_name, user_last_name, user_address, user_email, user_phone, user_username, user_password, user_role) VALUES ('348972198' , 'John', 'Smith', '1234 Main St, Athens, Greece', 'john.smith@gmail.com', '2101234567', 'john_smith', '$2a$10$OUfbSdSEIkYu62W5bfkp.OBas2KAZke.Gxk43Avwht0hGqUpOzILG', 'Owner')
--INSERT INTO USER (user_ssn, user_first_name, user_last_name, user_address, user_email, user_phone, user_username, user_password, user_role) VALUES ('348972199' , 'George', 'Papadopoulos', '4567 Main St, Athens, Greece', 'george.papadopoulos@gmail.com', '2101296457', 'george_papadopoulos', '456', 'Owner');
INSERT INTO USER (user_ssn, user_first_name, user_last_name, user_address, user_email, user_phone, user_username, user_password, user_role) VALUES ('348972199' , 'George', 'Papadopoulos', '4567 Main St, Athens, Greece', 'george.papadopoulos@gmail.com', '2101296457', 'george_papadopoulos', '$2a$10$mON7hb9zSaKu7gggIkxNweGkZRk81oAwcrDWSzpIdVxfdRO2YoBzi', 'Owner');
--INSERT INTO USER (user_ssn, user_first_name, user_last_name, user_address, user_email, user_phone, user_username, user_password, user_role) VALUES ('098542318' , 'Alexander', 'Bolt', '2392 Main St, Patras, Greece', 'alex@gmail.com', '2610996524', 'alexander_bolt', '20332033', 'Admin');
INSERT INTO USER (user_ssn, user_first_name, user_last_name, user_address, user_email, user_phone, user_username, user_password, user_role) VALUES ('098542318' , 'Alexander', 'Bolt', '2392 Main St, Patras, Greece', 'alex@gmail.com', '2610996524', 'admin', '$2a$10$PWE0Wy9EBBST9tVMtMf/4.YieKNxbukEq4SLWnRS7qlJuw5bs7KQG', 'Admin');
INSERT INTO USER (user_ssn, user_first_name, user_last_name, user_address, user_email, user_phone, user_username, user_password, user_role) VALUES ('384925389' , 'Jason', 'Wayne', '2278 Main St, Athens, Greece', 'j.wayne@gmail.com', '2105589001', 'j_wayne', '1234', 'Owner');
INSERT INTO USER (user_ssn, user_first_name, user_last_name, user_address, user_email, user_phone, user_username, user_password, user_role) VALUES ('556878932' , 'Luke', 'Bolt', '5779 Main St, Athens, Greece', 'L.B@gmail.com', '2104478970', 'luke_bolt', '19901990', 'Owner');
INSERT INTO USER (user_ssn, user_first_name, user_last_name, user_address, user_email, user_phone, user_username, user_password, user_role) VALUES ('909754361' , 'Mary', 'Johnson', '9902 Main St, Athens, Greece', 'mary.johnson@gmail.com', '2102044452', 'm_johnson', '987654321', 'Owner');
INSERT INTO USER (user_ssn, user_first_name, user_last_name, user_address, user_email, user_phone, user_username, user_password, user_role) VALUES ('219812345' , 'Thomas', 'Robles', '8761 Main St, Athens, Greece', 'T.robles@gmail.com', '2107709532', 't_robles', '12341234', 'Owner');
INSERT INTO USER (user_ssn, user_first_name, user_last_name, user_address, user_email, user_phone, user_username, user_password, user_role) VALUES ('469570126' , 'Philip', 'Anderson', '9012 Main St, Patras, Greece', 'philip.a@gmail.com', '2610436799', 'philip_a', '1111', 'Owner');
INSERT INTO USER (user_ssn, user_first_name, user_last_name, user_address, user_email, user_phone, user_username, user_password, user_role) VALUES ('358500986' , 'Socrates', 'Rondulescu', '9902 Main St, Patras, Greece', 'socrates.r@gmail.com', '2610990512', 'socrates_ron', '12121212', 'Owner');
INSERT INTO USER (user_ssn, user_first_name, user_last_name, user_address, user_email, user_phone, user_username, user_password, user_role) VALUES ('129063002' , 'Jason', 'Timberlake', '8008 Main St, Patras, Greece', 'jason.tim@gmail.com', '2610077732', 'jason_tim', '11991199', 'Owner');
INSERT INTO USER (user_ssn, user_first_name, user_last_name, user_address, user_email, user_phone, user_username, user_password, user_role) VALUES ('446906418' , 'Luke', 'Iglesias', '1239 Main St, Patras, Greece', 'Luke.ig@gmail.com', '2610763802', 'luke_ig', '56785678', 'Owner');




INSERT INTO REPAIR (repair_date,repair_address, repair_cost, repair_description, repair_owner, repair_status, repair_category, repair_type) VALUES ('2020-12-12 12:34:56', '1234 Main St, Athens, Greece' , 2500, 'Wall and Ceiling', '348972198', 'Pending', 'Interior','Electrical');
INSERT INTO REPAIR (repair_date,repair_address, repair_cost, repair_description, repair_owner, repair_status, repair_category, repair_type) VALUES ('2020-12-12 12:34:56', '1234 Main St, Athens, Greece' , 2500, 'Wall and Ceiling', '348972198', 'Pending', 'Interior','Electrical');
INSERT INTO REPAIR (repair_date,repair_address, repair_cost, repair_description, repair_owner, repair_status, repair_category, repair_type) VALUES ('2020-12-12 12:34:56', '1234 Main St, Athens, Greece' , 2500, 'Wall and Ceiling', '348972198', 'Pending', 'Interior','Electrical');
INSERT INTO REPAIR (repair_date,repair_address, repair_cost, repair_description, repair_owner, repair_status, repair_category, repair_type) VALUES ('2018-12-30 12:16:10', '4567 Main St, Athens, Greece' , 3000, 'Wall and Ceiling', '348972198', 'In Progress', 'Interior', 'Painting');
INSERT INTO REPAIR (repair_date,repair_address, repair_cost, repair_description, repair_owner, repair_status, repair_category, repair_type) VALUES ('2018-12-30 12:16:10', '4567 Main St, Athens, Greece' , 3000, 'Wall and Ceiling', '348972198', 'In Progress', 'Interior', 'Painting');
INSERT INTO REPAIR (repair_date,repair_address, repair_cost, repair_description, repair_owner, repair_status, repair_category, repair_type) VALUES ('2018-12-30 12:16:10', '4567 Main St, Athens, Greece' , 3000, 'Wall and Ceiling', '348972198', 'In Progress', 'Interior', 'Painting');
INSERT INTO REPAIR (repair_date,repair_address, repair_cost, repair_description, repair_owner, repair_status, repair_category, repair_type) VALUES ('2018-12-30 12:16:10', '4567 Main St, Athens, Greece' , 3000, 'Wall and Ceiling', '348972198', 'In Progress', 'Interior', 'Painting');
INSERT INTO REPAIR (repair_date,repair_address, repair_cost, repair_description, repair_owner, repair_status, repair_category, repair_type) VALUES ('2018-12-30 12:16:10', '4567 Main St, Athens, Greece' , 3000, 'Wall and Ceiling', '348972198', 'Completed', 'Interior', 'Painting');
INSERT INTO REPAIR (repair_date,repair_address, repair_cost, repair_description, repair_owner, repair_status, repair_category, repair_type) VALUES ('2018-12-30 12:16:11', '4567 Main St, Athens, Greece' , 3001, 'Wall and Ceiling', '348972199', 'In Progress', 'Interior', 'Waterproofing');
INSERT INTO REPAIR (repair_date,repair_address, repair_cost, repair_description, repair_owner, repair_status, repair_category, repair_type) VALUES ('2020-12-31 12:34:56', '1234 Main St, Athens, Greece' , 3000, 'Wall and Ceiling', '348972198', 'In Progress', 'Interior', 'Painting');
INSERT INTO REPAIR (repair_date,repair_address, repair_cost, repair_description, repair_owner, repair_status, repair_category, repair_type) VALUES ('2020-12-16 12:55:06', '3624 Main St, Athens, Greece' , 2500, 'Wall and Ceiling', '212539098', 'Completed', 'Interior', 'Painting');
INSERT INTO REPAIR (repair_date,repair_address, repair_cost, repair_description, repair_owner, repair_status, repair_category, repair_type) VALUES ('2020-12-17 09:24:56', '2004 Main St, Athens, Greece' , 3500, 'Ceiling', '347409698', 'Pending', 'Interior', 'Insulation');
INSERT INTO REPAIR (repair_date,repair_address, repair_cost, repair_description, repair_owner, repair_status, repair_category, repair_type) VALUES ('2020-12-17 12:00:00', '1009 Main St, Athens, Greece' , 3000, 'Wall', '300783619', 'In Progress', 'Interior', 'Chambranle');
INSERT INTO REPAIR (repair_date,repair_address, repair_cost, repair_description, repair_owner, repair_status, repair_category, repair_type) VALUES ('2020-12-18 13:04:56', '1734 Main St, Athens, Greece' , 3000, 'Bathroom', '858904673', 'In Progress', 'Interior', 'Plumbing');
INSERT INTO REPAIR (repair_date,repair_address, repair_cost, repair_description, repair_owner, repair_status, repair_category, repair_type) VALUES ('2020-12-19 17:30:02', '9903 Main St, Athens, Greece' , 2000, 'Living Room', '994678203', 'Completed', 'Interior', 'Electrical Work');
INSERT INTO REPAIR (repair_date,repair_address, repair_cost, repair_description, repair_owner, repair_status, repair_category, repair_type) VALUES ('2020-12-20 10:54:56', '5604 Main St, Athens, Greece' , 3500, 'Wall and Ceiling', '177790598', 'Pending', 'Interior', 'Painting');



INSERT INTO PROPERTY(property_pin, property_address, property_year, property_type, property_owner) VALUES ('12345', 'Kifisias 12', 2018, 'Maisonette', '348972198');
INSERT INTO PROPERTY(property_pin, property_address, property_year, property_type, property_owner) VALUES ('12346', 'Kifisias 12', 2018, 'Maisonette', '348972199');
INSERT INTO PROPERTY(property_pin, property_address, property_year, property_type, property_owner) VALUES ('12347', 'Kifisias 12', 2018, 'Maisonette', '348972200');
INSERT INTO PROPERTY(property_pin, property_address, property_year, property_type, property_owner) VALUES ('12348', 'Kifisias 12', 2018, 'Maisonette', '348972201');
INSERT INTO PROPERTY(property_pin, property_address, property_year, property_type, property_owner) VALUES ('12349', 'Kifisias 12', 2018, 'Maisonette', '348972198');
